const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')

if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config()
}

const app = express()
const port = process.env.PORT

app.set('views', path.join(__dirname, 'app/views'))
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('app/public'))

app.use('/', require('./app/routes'))

app.listen(port, err => {
  if (err) {
    console.log('Error')
  } else {
    console.log(`Server in running on port ${port}`)
  }
})
