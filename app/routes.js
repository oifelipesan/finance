const express = require('express')
const routes = express.Router()
const axios = require('axios')

const { KEY, BRAND } = require('../config/config.json')
const baseUrl = `https://api.hgbrasil.com/finance?key=${KEY}`

routes.get('/', async (req, res) => {
  const content = await axios.get(baseUrl)
  const cotacao = content.data.results.currencies
  const bolsa = content.data.results.stocks
  const bitcoin = content.data.results.bitcoin
  const taxas = content.data.results.taxes

  res.render('index', {
    page: 'pages/home',
    title: BRAND,
    cotacao,
    bolsa,
    bitcoin,
    taxas
  })
})

routes.get('/home2', async (req, res) => {
  const content = await axios.get(baseUrl)
  const cotacao = content.data.results.currencies
  const bolsa = content.data.results.stocks
  const bitcoin = content.data.results.bitcoin
  const taxas = content.data.results.taxes

  res.render('index', {
    page: 'pages/home2',
    title: BRAND,
    cotacao,
    bolsa,
    bitcoin,
    taxas
  })
})

module.exports = routes
