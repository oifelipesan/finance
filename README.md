[![N|Solid](https://camo.githubusercontent.com/89b323a471305f437f6e16852b5be19e670525a0/68747470733a2f2f6465706c6f792e6e6f772e73682f7374617469632f627574746f6e2e737667)](https://zeit.co/) [![LICENSE](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/fsantos.san/finance/blob/master/LICENSE) [![Gitlab pipeline status (branch)](https://img.shields.io/gitlab/pipeline/fsantos.san/finance/master.svg)](https://gitlab.com/fsantos.san/finance)

# Finance

Finance, this is a basic application to consume [HG Finance API](https://hgbrasil.com/status/finance/), which is an API in which it provides financial data with the focus on bringing the amount of information to a single request.

## Tech

Finance uses several technologies for its operation

- [EJS](https://ejs.co/) - Embedded JavaScript templates
- [Skeleton](http://getskeleton.com/) - Great UI boilerplate for modern web apps
- [node.js](https://nodejs.org/) - evented I/O for the backend
- [Express](https://expressjs.com/) - fast node.js network app framework
- [Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
- [body-parser](https://github.com/expressjs/body-parser) - Node.js body parsing middleware

And of course Finance itself is open source with a [public repository](https://gitlab.com/fsantos.san/finance)
on GitLab.

### Installation

Finance requires [Node.js](https://nodejs.org/) v10+ to run.

Enter the project folder:

```
$ cd finance
```

Install the dependencies and devDependencies and start the server.

```
$ yarn install
$ yarn start
```

or

```
$ npm install
$ npm start
```

**The Open Way of thinking!**
